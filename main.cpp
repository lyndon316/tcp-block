#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>
#include <errno.h>
#include <cstring>
#include <signal.h>
#include <libnet.h>
#include <sys/ioctl.h>
#include <pcap.h>
#include <net/if.h>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdint.h>

#include <stdbool.h>

#include "ethhdr.h"
#include "ip.h"

typedef struct psuedo_hdr{
    Ip src_ip;
    Ip dst_ip;
    uint8_t placeholder;
    uint8_t protocol;
    uint16_t tcp_len;
} psuedo_hdr;



#define MAX_PATTERN_LEN 316
#pragma pack(push, 1)
typedef struct asbal{
    struct libnet_ethernet_hdr eth_hdr;
    struct libnet_ipv4_hdr ip_hdr;
    struct libnet_tcp_hdr tcp_hdr;
    uint8_t data[157];
}tcp_pk_hdr;
#pragma pack(pop)


#pragma pack(push, 1)
struct data_packet
{
	u_char asdf[157];
};
#pragma pack(pop)

void printxxd(u_char* arr, int n)
{
	for(int i = 0; i < n; i++)
	{
		printf("%02x ", arr[i]);	
	}
}




char pattern[MAX_PATTERN_LEN];
int pattern_len;
pcap_t* handle;
char backward_msg[] = "HTTP/1.0 302 Redirect\r\nLocation: http://warning.or.kr\r\n\r\n";
Mac attacker_mac;


uint16_t ip_checksum(libnet_ipv4_hdr ip_hdr_p)
{
	uint16_t *split_iphdr = (uint16_t *)&ip_hdr_p;
	uint32_t sum = 0;
	for (int i = 0; i < 10; i++)
	{
		sum += ntohs(split_iphdr[i]);
	}
	while (sum >> 16)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	return htons((uint16_t)~sum);
}


uint16_t tcp_checksum(tcp_pk_hdr *packet, int size)
{
	packet->tcp_hdr.th_sum = 0;
	psuedo_hdr psuedo_hdr;
	psuedo_hdr.src_ip = Ip(packet->ip_hdr.ip_src.s_addr);
	psuedo_hdr.dst_ip = Ip(packet->ip_hdr.ip_dst.s_addr);
	psuedo_hdr.placeholder = 0;
	psuedo_hdr.protocol = packet->ip_hdr.ip_p;
	psuedo_hdr.tcp_len = ntohs(size);
	uint32_t sum = 0;
	uint16_t *psuedo_hdr16 = (uint16_t *)&psuedo_hdr;
	for (int i = 0; i < 6; i++)
	{
		sum += ntohs(psuedo_hdr16[i]);
	}
	while (sum >> 16)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	uint16_t *tcp16 = (uint16_t *)&packet->tcp_hdr;
	for (int i = 0; i < size / 2; i++)
	{
		sum += ntohs(tcp16[i]);
	}
	while (sum >> 16)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	if (size % 2 == 1)
	{
		sum += ntohs(tcp16[size / 2]);
	}
	return htons((uint16_t)~sum);
}


int searchData(u_char* data, int data_len)
{
	//printf("%d %d\n", data_len, pattern_len);
	for(int i = 0; i < 32; i++)
	{
		if(memcmp(&data[i], &pattern[0], pattern_len) == 0)
		{
			//printf("here it is\n");
			return 1;
		}
	}
	return 0;
}

int forward(libnet_ethernet_hdr ethernet_header, libnet_ipv4_hdr ip_header, libnet_tcp_hdr tcp_header, int dsize)
{
	tcp_pk_hdr* packet = (tcp_pk_hdr*)malloc(sizeof(tcp_pk_hdr));
	memset(packet, 0, sizeof(tcp_pk_hdr));
    packet->eth_hdr = ethernet_header;
    packet->ip_hdr = ip_header;

    //printf("%d %x %x\n", sizeof(packet.eth_hdr), &packet.ip_hdr, &packet.eth_hdr);
    //printxxd((u_char*)&packet, 32);
    //printf("\n");
    packet->tcp_hdr = tcp_header;
    packet->tcp_hdr.th_off = 5;
    packet->ip_hdr.ip_len = htons(packet->ip_hdr.ip_hl * 4 + packet->tcp_hdr.th_off * 4);
	//packet->tcp_hdr.th_seq = htonl(ntohl(packet->tcp_hdr.th_seq) + dsize);
    packet->tcp_hdr.th_flags = TH_RST | TH_ACK;
    packet->tcp_hdr.th_win = 0;

    //uint8_t* whynot = &attacker_mac;
    //memcpy(&(packet->eth_hdr.ether_shost), reinterpret_cast<const u_char*>(&attacker_mac), 6);

    //printxxd((u_char*)&packet, 32);
	//packet.eth_hdr.ether_shost = attacker_mac;

	packet->ip_hdr.ip_sum = ip_checksum(packet->ip_hdr);
	packet->tcp_hdr.th_sum = tcp_checksum(packet, packet->tcp_hdr.th_off * 4);

    //pcap_inject(handle, reinterpret_cast<const u_char*>(&packet), ntohs(packet.ip_hdr.ip_len)+LIBNET_ETH_H);
    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(packet), ntohs(packet->ip_hdr.ip_len)+LIBNET_ETH_H);
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        return -1;
    }
    return 0;
}



int backward(libnet_ethernet_hdr ethernet_header, libnet_ipv4_hdr ip_header, libnet_tcp_hdr tcp_header, int dsize)
{
	uint32_t temp32;
	uint16_t temp16;
	in_addr in_temp;
	tcp_pk_hdr* packet = (tcp_pk_hdr*)malloc(sizeof(tcp_pk_hdr));
	memset(packet, 0, sizeof(tcp_pk_hdr));
    packet->eth_hdr = ethernet_header;
    //printf("%d %x %x\n", sizeof(packet.eth_hdr), &packet.ip_hdr, &packet.eth_hdr);
    packet->ip_hdr = ip_header;
    packet->tcp_hdr = tcp_header;
    packet->tcp_hdr.th_off = 5;
    packet->ip_hdr.ip_len = htons(packet->ip_hdr.ip_hl * 4 + packet->tcp_hdr.th_off * 4 + strlen(backward_msg));

	packet->tcp_hdr.th_seq = packet->tcp_hdr.th_seq + htonl(dsize);
    packet->tcp_hdr.th_flags = TH_FIN | TH_ACK;
    temp32 = packet->tcp_hdr.th_seq;
    packet->tcp_hdr.th_seq = packet->tcp_hdr.th_ack;
    packet->tcp_hdr.th_ack = temp32;

    temp16 = packet->tcp_hdr.th_dport;
    packet->tcp_hdr.th_dport = packet->tcp_hdr.th_sport;
    packet->tcp_hdr.th_sport = temp16;

	//packet.eth_hdr.ether_dhost = packet.eth_hdr.ether_shost;
	//packet.eth_hdr.ether_shost = attacker_mac;
	memcpy(&(packet->eth_hdr.ether_dhost), &(packet->eth_hdr.ether_shost), 6);
	memcpy(&(packet->eth_hdr.ether_shost), reinterpret_cast<const u_char*>(&attacker_mac), 6);

	in_temp = packet->ip_hdr.ip_dst;
	packet->ip_hdr.ip_dst = packet->ip_hdr.ip_src;
	packet->ip_hdr.ip_src = in_temp;

    packet->ip_hdr.ip_ttl = 128;

    memcpy(&(packet->data), backward_msg, strlen(backward_msg));

    //afaaaaaaaaaaaaaaaaaaaaaasfioafeijo;ioajeroj
    packet->ip_hdr.ip_sum = ip_checksum(packet->ip_hdr);
	packet->tcp_hdr.th_sum = tcp_checksum(packet, packet->tcp_hdr.th_off * 4 + strlen(backward_msg));

/*
    //pcap_inject(handle, reinterpret_cast<const u_char*>(&packet), ntohs(packet.ip_hdr.ip_len)+LIBNET_ETH_H);
    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(packet), ntohs(packet->ip_hdr.ip_len)+LIBNET_ETH_H);
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        return -1;
    }*/

    int sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
	if (sockfd < 0)
	{
		printf("socket fail\n");
		return -1;
	}

	int no = 1;
	if (setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, &no, sizeof(no)) < 0)
	{
		printf("setsockopt fail\n");
		return -1;
	}

	struct sockaddr_in sockaddr_1;
	sockaddr_1.sin_port = packet->tcp_hdr.th_dport;
	sockaddr_1.sin_addr.s_addr = packet->ip_hdr.ip_dst.s_addr;


	sockaddr_1.sin_family = AF_INET;
	char send_chars[4096];
	memset(send_chars, 0, sizeof(send_chars));
	struct libnet_ipv4_hdr *ip_hdr_p = (libnet_ipv4_hdr *)send_chars;
	memcpy(ip_hdr_p, &(packet->ip_hdr), sizeof(libnet_ipv4_hdr));

	struct libnet_tcp_hdr *tcpHeader = (libnet_tcp_hdr *)(send_chars + sizeof(libnet_ipv4_hdr));
	memcpy(tcpHeader, &(packet->tcp_hdr), sizeof(libnet_tcp_hdr));

	char *payload = send_chars + sizeof(libnet_ipv4_hdr) + sizeof(libnet_tcp_hdr);
	strcpy(payload, backward_msg);
	//aaaa
	if (sendto(sockfd, send_chars, ntohs(ip_hdr_p->ip_len), 0, (struct sockaddr *)&sockaddr_1, sizeof(sockaddr_1)) < 0)
	{
		printf("send fail\n");
		return -1;
	}

    return 0;
}

void printMac(uint8_t* mac_arr)
{
	for(int i = 0; i < ETHER_ADDR_LEN - 1; i++)
	{
		printf("%02x::", mac_arr[i]);
	}
	printf("%02x\n", mac_arr[ETHER_ADDR_LEN - 1]);
}

int main(int argc, char** argv)
{
	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];

	pattern_len = strlen(argv[2]);
	memcpy(pattern, argv[2], pattern_len + 2);

	std::ifstream iface("/sys/class/net/" + std::string(dev) + "/address");
  	std::string my_mac((std::istreambuf_iterator<char>(iface)), std::istreambuf_iterator<char>());

	printf("MyMac : %s\n", my_mac.c_str());
	attacker_mac = Mac(my_mac);

	handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if(handle == NULL)
	{
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", dev, errbuf);
		return -1;
	}
	struct pcap_pkthdr* headeraa;
	const u_char* packet = (u_char*)malloc(BUFSIZ);
	if(packet == NULL)
	{
		fprintf(stderr, "malloc return null\n");
		return -1;
	}
	//printf("before while complete\n");
	while (1) {
		int res = pcap_next_ex(handle, &headeraa, &packet);
		//printf("aa\n");
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			break;
		}

    	//printxxd((u_char*)&packet, 32);
    	//printf("\n");

		void* ethernet_header = (u_char*)packet;
		void* ip_header = (u_char*)packet + sizeof(libnet_ethernet_hdr);
		//printf("assign win\n");
		if(((libnet_ipv4_hdr*)ip_header)->ip_v != 4)
			continue;
		if(((libnet_ipv4_hdr*)ip_header)->ip_p != 6)
			continue;
		void* tcp_header = (u_char*)ip_header + ((libnet_ipv4_hdr*)ip_header)->ip_hl * 4;
		void* data = (u_char*)tcp_header + ((libnet_tcp_hdr*)tcp_header)->th_off * 4;

		int dsize = ntohs(((libnet_ipv4_hdr*)ip_header)->ip_len) - ((libnet_ipv4_hdr*)ip_header)->ip_hl * 4 - ((libnet_tcp_hdr*)tcp_header)->th_off * 4;
		if(dsize < 0)
		{
			fprintf(stderr, "packet parsing error : header size is larger than packet\n");
			continue;
		}

		if(searchData(((data_packet*)data)->asdf, dsize) == 0)
		{
			//printf("not malicious\n");
			continue;
		}
		printf("malicious!\n");
		//processing malicious packet
		forward(*((libnet_ethernet_hdr*)ethernet_header), *((libnet_ipv4_hdr*)ip_header), *((libnet_tcp_hdr*)tcp_header), dsize);
		//printf("forward end\n");
		backward(*(libnet_ethernet_hdr*)ethernet_header, *(libnet_ipv4_hdr*)ip_header, *(libnet_tcp_hdr*)tcp_header, dsize);

	}

}